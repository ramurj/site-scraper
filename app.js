
/**
 * Module dependencies.
 */
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');


console.log('Starting site scraper');
fs.stat('./tarots', function(err,stats){
	if(!err){
		if(stats.isDirectory())
			console.log('Dir found');
	}
	else {
		fs.mkdir('./tarots', function(err) {
			if (err)
				console.log('error creating dir ' + err);
		});
	}
});
var nr = 3;
for(i=1;i<nr;i++){
	var url = 'http://www.osho.com/magazine/tarot/TarotCardNew.cfm?All=Yes&Nr=' + i;
	request(url, function(err, resp, body){
		$ = cheerio.load(body);
		heading = $('h5').text();
		
		img = $('p table img').attr('src');
		//img_name = path.basename(img);
		console.log("Saving image " + heading);
		request("http://www.osho.com/magazine/tarot/" + img).pipe(fs.createWriteStream('./tarots/' + heading + ".jpg"));
		links = $('table .text '); //use your CSS selector here
		
		console.log("Writing page " + heading)
		var log = fs.createWriteStream('./tarots/' + heading, {'flags': 'a'});
		log.write(heading + "\n\n");
		log.write("Description \n" + $(links[0]).text() + "\n\n");
		log.write("Commentary \n" + $(links[1]).text() + "\n\n");
		log.on('error', function (err) {
				console.log(err);
		});
	});
}

